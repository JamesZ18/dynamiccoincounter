﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCoinCounter
{
    class Solutions
    {
        private int[] values = { 1, 5, 10, 25, 50, 100 };
        private int[,] numCombinations;
        private int totalSum = 0;

        public Solutions (int sum)
        {
            this.totalSum = sum;
            this.numCombinations = new int[values.Length, sum];
            initializeArray(values.Length, sum);

        }

        public int TotalPermutations()
        {
            return GetPermutations(totalSum);
        }

        public int GetPermutations(int sum, int index = 5)
        {
            //Console.WriteLine($"Entering Get Permutations: {index}, {sum}");
            if (sum == 0)
            {
                return 1;
            }
            else if (sum < 0)
            {
                return 0;
            }
            if (index < 0)
            {
                return 0;
            }


            if (this.numCombinations[index, sum - 1] == -1)
            {
                this.numCombinations[index, sum - 1] = FindPermutations(sum, index);               
            }

            return this.numCombinations[index, sum - 1];
        }

        private void initializeArray(int length, int initialSum)
        {
            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < initialSum; j++)
                {
                    this.numCombinations[i, j] = -1;
                }
            }
        }

        public int FindPermutations(int sum, int index)
        {            
            int newSum = sum - values[index];
            int newIndex = index - 1;
            int PermutationsWithCoin = GetPermutations(newSum, index);
            int PermutationsWithoutCoin = GetPermutations(sum, newIndex);
            return PermutationsWithCoin + PermutationsWithoutCoin;
        }
    }
}
