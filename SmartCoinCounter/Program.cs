﻿using System;
using System.Diagnostics;

namespace SmartCoinCounter
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch watch = new Stopwatch();
            Console.WriteLine("Please enter the sum to find permutations: ");
            int initialSum;
            if (int.TryParse(Console.ReadLine(), out initialSum))
            {
                // Get time to complete with dynamic programming
                Console.WriteLine($"Starting stopwatch: {watch.ElapsedMilliseconds}");
                watch.Start();
                Solutions solutionObject = new Solutions(initialSum);
                Console.WriteLine("# Permutations: " + solutionObject.TotalPermutations());
                watch.Stop();
                Console.WriteLine($"Time taken: {watch.ElapsedMilliseconds}");
            }

            Console.WriteLine($"Starting stopwatch: {watch.ElapsedMilliseconds}");
            watch.Reset();
            watch.Start();
            Console.WriteLine($"Nondynamic: {LoopCounter.GetPermutations(initialSum)}");
            watch.Stop();
            Console.WriteLine($"Time taken: {watch.ElapsedMilliseconds}");
        }
    }
}
