﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace SmartCoinCounter
{
    static class LoopCounter
    {
        private static int[] values = { 1, 5, 10, 25, 50, 100 };
        private static int[] amounts = { 0, 0, 0, 0, 0, 0 };
        private static int numSolutions = 0;

        public static int GetPermutations(int sum)
        {
            int total = GetTotal();
            for (amounts[5] = 0; amounts[5] <= sum / values[5]; amounts[5]++)
            {
                for (amounts[4] = 0; amounts[4] <= sum / values[4]; amounts[4]++)
                {
                    for (amounts[3] = 0; amounts[3] <= sum / values[3]; amounts[3]++)
                    {
                        for (amounts[2] = 0; amounts[2] <= sum / values[2]; amounts[2]++)
                        {
                            for (amounts[1] = 0; amounts[1] <= sum / values[1]; amounts[1]++)
                            {
                                for (amounts[0] = 0; amounts[0] <= sum / values[0]; amounts[0]++)
                                {
                                    total = GetTotal();
                                    if (total == sum)
                                    {
                                        numSolutions++;
                                        if (numSolutions % 1000000 == 0)
                                        {
                                            Console.WriteLine(numSolutions);
                                        }
                                    }
                                    else if (total > sum)
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return numSolutions;
        }

        private static int GetTotal()
        {
            int total = 0;
            for (int i = 0; i < values.Length; i++)
            {
                total += values[i] * amounts[i];
            }
            return total;
        }
    }
}
